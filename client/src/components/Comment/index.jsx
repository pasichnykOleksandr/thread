import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Card } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user, likeCount }, likeComment }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
    </CommentUI.Content>
    <Card.Content extra>
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
        <Icon name="thumbs up" />
        {likeCount || 0}
      </Label>
      <Label basic size="small" as="a" className={styles.toolbarBtn}>
        <Icon name="thumbs down" />
        {0}
      </Label>
    </Card.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired
  // dislikeComment: PropTypes.func.isRequired
};

export default Comment;
