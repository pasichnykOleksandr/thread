import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import * as authService from 'src/services/authService';

// import * as userService from '../../../../server/src/api/services/userService';

import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
  // SET_ALL_COMMENTS
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

// const setCommentsAction = comments => ({
//   type: SET_ALL_COMMENTS,
//   comments
// });

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadNotUserPosts = filter => async dispatch => {
  const id = filter.userId;
  const allPosts = await postService.getAllPosts();
  const result = allPosts.filter(el => el.userId !== id);
  dispatch(setPostsAction(result));
};

export const loadPostsLikedByMe = filter => async dispatch => {
  // const id = filter.userId;
  const result = await postService.getAllReactions(filter);
  console.log(result);
  const posts = undefined;
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const sendMail = (mail, subject, body) => {
  window.open(`mailto:${mail}?subject=${subject}&body=${body}`);
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const currentPost = await postService.getPost(postId);
  const currentUser = await authService.getCurrentUser();
  const user = await authService.getUserById(currentPost.userId);
  // console.log(user);
  sendMail(user.email, 'Thread like',
    `Your post "${currentPost.body}" was liked by ${currentUser.username}`);
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
    // likeUsers: postService.getAllPostLikes(postId)
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

// export const likeComment = commentId => async (dispatch, getRootState) => {
export const likeComment = commentId => async () => {
  const { id } = await commentService.likeComment(commentId);
  // console.log(await postService.getPost(postId));
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff // diff is taken from the current closure
    // likeUsers: commentService.getAllPostLikes(postId)
  });

  console.log(mapLikes);
  // const { comments: { comments } } = getRootState();
  // const updated = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  // dispatch(setCommentsAction(updated));

  // if (expandedComment && expandedComment.id === commentId) {
  //   dispatch(setExpandedCommentAction(mapLikes(expandedComment)));
  // }
};
